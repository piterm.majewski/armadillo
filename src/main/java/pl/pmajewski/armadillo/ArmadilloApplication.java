package pl.pmajewski.armadillo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArmadilloApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArmadilloApplication.class, args);
    }

}
